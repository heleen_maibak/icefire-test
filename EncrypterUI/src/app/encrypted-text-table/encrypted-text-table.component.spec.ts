import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncryptedTextTableComponent } from './encrypted-text-table.component';

describe('EnrcyptedTextTableComponent', () => {
  let component: EncryptedTextTableComponent;
  let fixture: ComponentFixture<EncryptedTextTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncryptedTextTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncryptedTextTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
