import {Component, OnInit} from '@angular/core';

import {EncryptedItem} from '../shared/EncryptedItem';
import {EncryptedItemService} from '../services/encrypted-items.service';

@Component({
  selector: 'app-encrypted-text-table',
  templateUrl: './encrypted-text-table.component.html',
  styleUrls: ['./encrypted-text-table.component.scss']
})
export class EncryptedTextTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'encryptedText', 'decrypt'];
  encryptedItems: EncryptedItem[];

  constructor(private encryptedItemsService: EncryptedItemService) {
  }

  ngOnInit() {
    console.log ('HERE');
    this.encryptedItemsService.getEncryptedItems().subscribe(data => {
      this.encryptedItems = data;
    });
  }

  decryptText(id: number): void {
    console.log('decrypt, id=' + id);
  }
}
