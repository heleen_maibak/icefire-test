import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncryptDecryptFormComponent } from './encrypt-decrypt-form.component';

describe('EncryptDecryptFormComponent', () => {
  let component: EncryptDecryptFormComponent;
  let fixture: ComponentFixture<EncryptDecryptFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncryptDecryptFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncryptDecryptFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
