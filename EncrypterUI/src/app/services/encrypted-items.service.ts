import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EncryptedItem} from '../shared/EncryptedItem';
import { Observable, of} from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

const apiUrl = 'http://localhost:8080/encryptedItems';
@Injectable({
  providedIn: 'root'
})
export class EncryptedItemService {

  constructor(private http: HttpClient) {
  }

  public getEncryptedItems(): Observable<EncryptedItem[]> {
    return this.http.get<EncryptedItem[]>(apiUrl)
      .pipe(tap(encryptedItems => console.log(encryptedItems)),
        catchError(this.handleError('getEncryptedItems', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
