export class EncryptedItem {
  id: number;
  encryptedText: string;
}
