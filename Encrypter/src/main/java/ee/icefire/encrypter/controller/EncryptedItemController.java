package ee.icefire.encrypter.controller;

import ee.icefire.encrypter.entity.EncryptedItem;
import ee.icefire.encrypter.repository.EncryptedItemRepository;
import ee.icefire.encrypter.service.IEncryptedItemService;
import org.hibernate.query.QueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EncryptedItemController {
    @Autowired
    private IEncryptedItemService encryptedItemService;

    public EncryptedItemController() {
    }

    @GetMapping("/encryptedItems")
    public List<EncryptedItem> getEncryptedItems() {
        return encryptedItemService.getAllEncryptedItems();
    }

    @PostMapping("/encryptedItems")
    public List<EncryptedItem> encryptText(@RequestBody String plainText) {
        return encryptedItemService.addEncryptedItem(plainText);
    }
}
