package ee.icefire.encrypter;

import ee.icefire.encrypter.entity.EncryptedItem;
import ee.icefire.encrypter.repository.EncryptedItemRepository;
import ee.icefire.encrypter.util.EncryptDecrypt;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;

@SpringBootApplication
@EnableJpaRepositories("ee.icefire.encrypter.repository")
public class Encrypter {

    public static void main(String[] args) {
        SpringApplication.run(Encrypter.class, args);
    }

    @Bean
    CommandLineRunner init(EncryptedItemRepository encryptedItemRepository) throws NoSuchAlgorithmException {
        SecretKey key = EncryptDecrypt.generateSymmetricKey();

        return args -> {
            Stream.of(
                    EncryptDecrypt.encryptTextWithSymmetricKey(key.getEncoded(),"John".getBytes()),
                    EncryptDecrypt.encryptTextWithSymmetricKey(key.getEncoded(),"Julie".getBytes()),
                    EncryptDecrypt.encryptTextWithSymmetricKey(key.getEncoded(),"Jennifer".getBytes()),
                    EncryptDecrypt.encryptTextWithSymmetricKey(key.getEncoded(),"Helen".getBytes()),
                    EncryptDecrypt.encryptTextWithSymmetricKey(key.getEncoded(),"Rachel".getBytes())
            ).forEach(encryptedText -> {
                EncryptedItem item = new EncryptedItem(encryptedText);
                encryptedItemRepository.save(item);
            });
            encryptedItemRepository.findAll().forEach(System.out::println);
        };
    }
}
