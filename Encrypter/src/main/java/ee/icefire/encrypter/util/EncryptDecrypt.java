package ee.icefire.encrypter.util;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class EncryptDecrypt {
    private static final int SECRET_KEYSIZE = 2048;
    private static final String SECRET_KEY_ALGORITHM_TYPE = "RSA";

    private static final int SYMMETRIC_KEYSIZE = 256;
    private static final String SYMMETRIC_ALGORITHM_TYPE = "AES";
    public static final String SECURE_RANDOM_ALGORITHM = "DRBG";

    public static SecretKey generateSymmetricKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance(SYMMETRIC_ALGORITHM_TYPE);
        keyGen.init(SYMMETRIC_KEYSIZE);
        return keyGen.generateKey();
    }

    public static byte[] encryptTextWithSymmetricKey(byte[] key, byte[] inputText) throws Exception {
        SecretKey originalKey = getSecretKeyFromEncodedKey(key);
        return encryptDecrypt(SYMMETRIC_ALGORITHM_TYPE, originalKey, Cipher.ENCRYPT_MODE, inputText);
    }

    public static byte[] decryptTextWithSymmetricKey(byte[] key, byte[] encryptedText) throws Exception {
        SecretKey originalKey = getSecretKeyFromEncodedKey(key);
        return encryptDecrypt(SYMMETRIC_ALGORITHM_TYPE, originalKey, Cipher.DECRYPT_MODE, encryptedText);
    }

    public static byte[] encryptSymmetricKey(byte[] publicKey, byte[] symmetricKey)
            throws Exception {
        PublicKey key = KeyFactory.getInstance(SECRET_KEY_ALGORITHM_TYPE)
                .generatePublic(new X509EncodedKeySpec(publicKey));
        return encryptDecrypt(SECRET_KEY_ALGORITHM_TYPE, key, Cipher.ENCRYPT_MODE, symmetricKey);
    }

    public static byte[] decryptSymmetricKey(byte[] privateKey, byte[] symmetricKeyEncrypted)
            throws Exception {
        PrivateKey key = KeyFactory.getInstance(SECRET_KEY_ALGORITHM_TYPE)
                .generatePrivate(new PKCS8EncodedKeySpec(privateKey));
        return encryptDecrypt(SECRET_KEY_ALGORITHM_TYPE, key, Cipher.DECRYPT_MODE, symmetricKeyEncrypted);
    }

    private static byte[] encryptDecrypt(String algorithmType, Key originalKey, int encryptMode, byte[] inputText) throws Exception {
        Cipher cipher = Cipher.getInstance(algorithmType);
        cipher.init(encryptMode, originalKey);
        return cipher.doFinal(inputText);
    }

    static SecretKey getSecretKeyFromEncodedKey(byte[] key) {
        return new SecretKeySpec(key, 0, key.length, SYMMETRIC_ALGORITHM_TYPE);
    }

    public static KeyPair generateSecretKeyPair()
            throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(SECRET_KEY_ALGORITHM_TYPE);
        keyGen.initialize(SECRET_KEYSIZE, SecureRandom.getInstance(SECURE_RANDOM_ALGORITHM));
        return keyGen.generateKeyPair();
    }

}
