package ee.icefire.encrypter.service;

import ee.icefire.encrypter.entity.EncryptedItem;
import ee.icefire.encrypter.repository.EncryptedItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EncryptedItemService implements IEncryptedItemService {
    @Autowired
    private EncryptedItemRepository encryptedItemRepository;

    @Override
    public List<EncryptedItem> getAllEncryptedItems() {
        List<EncryptedItem> encryptedItems = new ArrayList<>();
        encryptedItemRepository.findAll().forEach(encryptedItem -> encryptedItems.add(encryptedItem));
        return encryptedItems;
    }

    @Override
    public List<EncryptedItem> addEncryptedItem(String encryptedText) {
        encryptedItemRepository.save(new EncryptedItem(encryptedText.getBytes()));
        return getAllEncryptedItems();
    }
}
