package ee.icefire.encrypter.service;

import ee.icefire.encrypter.entity.EncryptedItem;

import java.util.List;

public interface IEncryptedItemService {
    List<EncryptedItem> getAllEncryptedItems();
    List<EncryptedItem> addEncryptedItem(String encryptedText);
}
