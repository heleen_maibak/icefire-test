package ee.icefire.encrypter.entity;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

public class EncryptedItemKeys {
    @OneToOne
    private EncryptedItem encryptedItem;
    private byte[] encryptedSymmetricKey;
    @OneToMany
    private KeyPair secretKey;

    public EncryptedItemKeys(EncryptedItem encryptedItem, byte[] encryptedSymmetricKey, KeyPair secretKey) {
        this.encryptedItem = encryptedItem;
        this.encryptedSymmetricKey = encryptedSymmetricKey;
        this.secretKey = secretKey;
    }

    public EncryptedItem getEncryptedItem() {
        return encryptedItem;
    }

    public void setEncryptedItem(EncryptedItem encryptedItem) {
        this.encryptedItem = encryptedItem;
    }

    public byte[] getEncryptedSymmetricKey() {
        return encryptedSymmetricKey;
    }

    public void setEncryptedSymmetricKey(byte[] encryptedSymmetricKey) {
        this.encryptedSymmetricKey = encryptedSymmetricKey;
    }

    public KeyPair getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(KeyPair secretKey) {
        this.secretKey = secretKey;
    }
}
