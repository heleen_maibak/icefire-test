package ee.icefire.encrypter.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EncryptedItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private byte[] encryptedText;

    public EncryptedItem() {
    }

    public EncryptedItem(byte[] encryptedText) {
        this.encryptedText = encryptedText;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getEncryptedText() {
        return encryptedText;
    }

    public void setEncryptedText(byte[] encryptedText) {
        this.encryptedText = encryptedText;
    }

    @Override
    public String toString() {
        return "EncryptedItem{" +
                "id=" + id +
                ", encryptedText='" + encryptedText + '\'' +
                '}';
    }
}
