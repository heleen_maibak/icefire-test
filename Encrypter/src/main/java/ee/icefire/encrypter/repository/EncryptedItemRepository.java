package ee.icefire.encrypter.repository;

import ee.icefire.encrypter.entity.EncryptedItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EncryptedItemRepository extends CrudRepository <EncryptedItem, Long> {
}
