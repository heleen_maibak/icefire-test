package ee.icefire.encrypter;
import ee.icefire.encrypter.util.EncryptDecryptTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(EncryptDecryptTest.class)
public class TestSuite {
}
