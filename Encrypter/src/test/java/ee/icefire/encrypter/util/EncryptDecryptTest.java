package ee.icefire.encrypter.util;

import org.junit.Test;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;

public class EncryptDecryptTest {
    private final String TEST_STRING = "This is text to be encrypted 2019!";
    private final SecretKey SYMMETRIC_KEY = EncryptDecrypt.generateSymmetricKey();
    private final KeyPair SECRET_KEY = EncryptDecrypt.generateSecretKeyPair();

    public EncryptDecryptTest() throws NoSuchAlgorithmException {
    }

    @Test
    public void testKeyDecryptionReversesKeyEncryption() throws Exception {
        SecretKey result =  EncryptDecrypt.getSecretKeyFromEncodedKey(EncryptDecrypt.decryptSymmetricKey(
                SECRET_KEY.getPrivate().getEncoded(),
                EncryptDecrypt.encryptSymmetricKey(SECRET_KEY.getPublic().getEncoded(), SYMMETRIC_KEY.getEncoded()))
        );
        assertEquals(SYMMETRIC_KEY, result);
    }

    @Test
    public void testTextDecryptionReversesTextEncryption() throws Exception {
        String resultString = new String(EncryptDecrypt.decryptTextWithSymmetricKey(
                SYMMETRIC_KEY.getEncoded(),
                EncryptDecrypt.encryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(), TEST_STRING.getBytes())
        ));
        assertEquals(TEST_STRING, resultString);
    }

    @Test
    public void testMultipleDecryptionReversesMultipleEncryption() throws Exception {
        byte[] multiEncrypted = EncryptDecrypt.encryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(),
                EncryptDecrypt.encryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(),
                        EncryptDecrypt.encryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(), TEST_STRING.getBytes())));

        String resultString = new String(EncryptDecrypt.decryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(),
                EncryptDecrypt.decryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(),
                        EncryptDecrypt.decryptTextWithSymmetricKey(SYMMETRIC_KEY.getEncoded(), multiEncrypted))));
        assertEquals(TEST_STRING, resultString);
    }
}
